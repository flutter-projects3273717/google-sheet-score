import 'package:flutter/material.dart';

import 'api/sheets/score_sheets_api.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await ScoreSheetsApi.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Google Sheets Score',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(body: Container()),
    );
  }
}
