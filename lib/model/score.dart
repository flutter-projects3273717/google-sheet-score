class ScoreFields {
  static final String id = 'ID';
  static final String time_in = "TIME IN";
  static final String time_out = "TIME OUT";
  static final String difference = "DIFFERENCE";

  static List<String> getFields() => [id, time_in, time_out, difference];
}
