import 'package:flutter/material.dart';
import 'package:google_sheet_score/model/score.dart';
import 'package:gsheets/gsheets.dart';

class ScoreSheetsApi {
  // Credentials for Google Cloud Project
  static const _credentials = r'''
{
  "type": "service_account",
  "project_id": "spherical-list-384913",
  "private_key_id": "17d875d268a71e116f927687e3cd4c49de85ca94",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC/aIje+lgZ15F+\nZyUoUWM3C0u57Qp7K9aOXoDZPCuth1+Er+UhV+1VLC1AMjnr8d3rqmWKWqYPkXVy\nn2jb1CRBr3nA1sjNDtSJkQm1IjOpgjlikaKQP+KpePz6D9QXF6/4b2NUCL+dhBBR\nIS6WQERk+/MAyhZH2x/VoiyP4Ix4PUp1tXWOvl56ivaAOTnpud8PGczNpB4ttO8z\n4HvNGXVaKM1FmynfapiUqgLSqKqVewxpnnvoeUFbZZTbF6tGijHx3VsNJl2b8B4/\nKnE4u0GOHMHPWCxhx7EptI2sZKpBXNwKSi/d7lVvYP5BHUfUXOqV1ZRZ5UCUJ5P+\n8RKZfkJdAgMBAAECggEAGc3axI4Pv4LKGLYzZ7E6hMAEmE77s+mP4MdJFpWKi+KC\nqY2EVc+X7Xy+/aG9IPENSnPr1tBBWpBGZaAA+msp7M2zxnVjl7rGmLnRYGEj1tfO\nqwUC20Xe5ZsBJn2+HtOGLndnIXvrx9f3J9YnL4FVgcmyPmqAFiAqiq9KKPwMOTn1\nC6G1tenY+coTZKNbhEiWFeqioDMW2cyxARbkm0QQbw1z1LYSLnjeRjhKjfnqpRtf\nhoOYOQ6ejXb4e9Q3LH63TJWjgN6YIVu9iNX1vALu0eybyjJnvfzglIQAfmp/PCxf\nOFkxxTtXkZZi/JMMEifFXGwvYzS1x+dpPn2RuOzh/wKBgQDfcKBTYSYal19ISSmT\niCYOVqoVMiiMfx8GyXKrOfmLq7LriYjSJ1YaIXQWap8YZNg+YGr7hzPtRQFFQl7I\nGFCPQtfXnAvVA3a214xQLidcdD1lzG3naYoGEnqZiCI/i68KjaA5Ngk1Ef8DrSbm\nmWPsu9ZpbmiZg6n7+/ez+sUtfwKBgQDbTPoW/XmfVDI89G4vuVXRMs5ww12UD6fk\ngqypMp93FN3eAHSterWVaQdga3s/IJaKKQCyR8b77t1BKv4Udg2FodqpWcad50Gp\n6jkjm0AeReqv8/83IeEnVFjWB3pOMrQZbj3amHej2xyk4CMUUdfpRM6u7Ivoi30/\nI1v0w6/2IwKBgDDnba/SK4zWZLxlhvwLaIjq3v8Uo1tc12Wc4cQoRD4VrmtvBss4\nYNgHSmk9nOSx7cJ5R/EVfoAIAmcuR6Bz8e3pK2S9erl8sJcERmmmMGdD6cxGGif0\nKYl6seq4qQZPhFhC9zaOZSWActhTJZiDKiO+WQdqaupurt3AIE78K/vxAoGBAL2q\nqM3DcjH71utjF/1MlM023NcoEMIifDmlTwGTF9odAPqleEQl0fCkAs5AhzqEy844\nQjLu40k3QMLBFTjLFUzTPFOloqMkxGTZf4utFVpZa+274KplMd8bEy4hD4WcUql/\n8B0kKq1brz1l4NENK+jDKuKdETARnEREJ57ctE3zAoGADS7l1ltJjFQoe6eoK+kz\nEbEPQFKFphFPEXUtQwON4wKQoB5tcs6Go8B9Ge6l9ZFbPiA7DoTPFR4Zv8QpHwEk\nkjwyeljNvmBre1mXfOB3LNoT6HJ6fB2NsBMlizTuyIt85DoybpubK3gLkvMPu5/w\nEKFjqHO5D1EeSZi7TsXK7ow=\n-----END PRIVATE KEY-----\n",
  "client_email": "google-sheet-score@spherical-list-384913.iam.gserviceaccount.com",
  "client_id": "109264214716600101364",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/google-sheet-score%40spherical-list-384913.iam.gserviceaccount.com"
}
''';

  // Google sheet url
  static const _spreadsheetId = '1BEpFyQogM28trmr4qFJ7JsZHZKZDiu40lxskXdPh9i8';

  // Initialize Google sheet Package
  static final _gsheets = GSheets(_credentials);

  static Worksheet? _scoreSheet;

  static DateTime date = DateTime.now();

  static Future init() async {
    try {
      final spreadsheet = await _gsheets.spreadsheet(_spreadsheetId);
      _scoreSheet = await _getWorkSheet(spreadsheet, title: '${date.month}');

      // Init first row of our sheet
      final firstRow = ScoreFields.getFields();
      // Save the first row
      _scoreSheet!.values.insertRow(1, firstRow);
    } catch (e) {
      debugPrint('Init Error: $e');
    }
  }

  static Future<Worksheet> _getWorkSheet(Spreadsheet spreadsheet, {required String title}) async {
    try {
      return await spreadsheet.addWorksheet(title);
    } catch (e) {
      return await spreadsheet.addWorksheet(title);
    }
  }
}
